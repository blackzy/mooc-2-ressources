# Fiche remédiation élève en difficulté


### Axe de travail N°1:

Reformulation du cours et des exercices par une approche plus concrète, basée sur des exemples.

### Axe de travail N°2:

Création de nouveaux support de transmission (audio, vidéo).

### Axe de travil N°3:

Réalisation de documents de travail adaptés au niveau de l'élève, et depot sur un espace public sur internet.


### Axe de travail N°4:

Proposition à l'élève de sources de travail réalisées par des confrères, permettant une approche pédagogique différente.
