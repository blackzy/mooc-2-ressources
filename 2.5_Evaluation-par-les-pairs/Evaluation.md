# Evaluation


### Exercice 1:

Écrire un programme dans lequel on associe la valeur 12 au nom point_de_force. 

La valeur associée à point_de_force devra ensuite être affichée dans la console.

### Exercice 2:

Ecrire un programme qui additionnera le contenu de 2 variables (nom des variables : a et b). 

Le résultat de cette opération devra être associé à une variable nommée resultat (atention pas d'accent dans les noms de variable).

### Exercice 3:

Quelles sont les valeurs associées aux noms suivants : d, e, f, g, h et i après l'exécution du programme suivant :


```python
	import math
	a = 5
	b = 16
	c = 3.14 / 2
	d = b / a
	e = b // a
	f = b % a
	g = math.pow(a,2)
	h = math.sqrt(b)
	i = math.sin(c)
```

### Exercice 4:

Codez en Python la fonction y = x2+2x+10

### Exercice 5:

Quel est le résultat attendu après l'exécution du programme ci-dessous et la saisie dans la console de "dit_bonjour("toto", 14)" ?


```python
	def dit_bonjour(nom, age):
		phrase = f"Bonjour {nom}, vous avez {age} ans."
		return phrase

```

### Exercice 6:

Soit le programme suivant :

```python
	a = 4
	b = 7
```
			
Quel est le résultat attendu après l'exécution de ce programme si vous saisissez dans la console "a==b" ?

### Exercice 7:

Soit le programme suivant :

```python
	a = 4
	b = 7
	if a < b:
		print("Je suis toto.");
		print("Je n'aime pas titi.")
	else:
		print("Je suis titi.")
		print("Je n'aime pas toto.")
	print("En revanche, j'aime le Python.")
````
			
Quel est le résultat attendu après l'exécution de ce programme ?

### Exercice 8:

Proposer un code qui affiche les 5 premières occurence de la suite S(n+1)=Sn*2


Sources: https://pixees.fr/informatiquelycee/n_site/nsi_prem_pythonbase.html

