- Thématique : Débuter en Python.

- Notions liées : Utilisation de la console - Gestion des fichiers.

- Résumé de l’activité : Cours accompagné d'exercices sur la prise en main du langage python, variables, fonctions, modules, conditions et itérations.

- Objectifs : Favoriser la connaissance de l'environnement de travail sous python qui sera utilisé tout au long de l'année. Premiers contact avec les fonctionnalités de base.

- Auteur : Gregory LENOIR

- Durée de l’activité : 4h

- Forme de participation : individuelle

- Matériel nécessaire : Ordinateur

- Préparation :

- Autres références : Fiche activité éleve: https://www.math93.com/images/pdf/NSI/premiere/NSI_Debuter_Python_V4_Partie_1.pdf
